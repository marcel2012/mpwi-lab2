#include <stdio.h>
#include <limits.h>
#include <stdbool.h>

unsigned int linear_random_generator() {
    const unsigned long long int slope = 69069, offset = 1, modulo = UINT_MAX;
    static unsigned int previous_value = 7;

    unsigned int random_value = (slope * previous_value + offset) % modulo;
    previous_value = random_value;

    return random_value;
}

unsigned int shift_register_random_generator() {
    const unsigned int first_bit_offset = 7, second_bit_offset = 3, number_of_bits = 32, initial_bits = 7;
    static unsigned int initial_value = 0b1000101;

    unsigned int random_value = initial_value;
    for (unsigned int bit_index = initial_bits; bit_index < number_of_bits; bit_index++) {
        unsigned int first_bit = (random_value >> (bit_index - first_bit_offset)) & 1;
        unsigned int second_bit = (random_value >> (bit_index - second_bit_offset)) & 1;
        unsigned int new_bit = first_bit ^second_bit;
        random_value |= new_bit << bit_index;
    }
    initial_value = random_value >> (number_of_bits - initial_bits);

    return random_value;
}

int main() {
    unsigned int (*random_generator)();

    while (true) {
        printf("Wybierz rodzaj generatora:\n");
        printf("1) Generator liniowy\n");
        printf("2) Generator rejestrowy\n");
        int selected_generator;
        scanf("%d", &selected_generator);
        if (selected_generator == 1) {
            random_generator = linear_random_generator;
            break;
        } else if (selected_generator == 2) {
            random_generator = shift_register_random_generator;
            break;
        }
    }

    for (int i = 0; i < 100000; i++) {
        printf("%u\n", random_generator());
    }
    return 0;
}
